//
//  ViewController.swift
//  tapper-game
//
//  Created by Juan Ramirez on 4/16/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //Variables
    var maxTapsAmount = 0
    var currentTaps = 0
    
    //Outlets 
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var howManyTapsTextField: UITextField!
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var tapButton: UIButton!
    @IBOutlet weak var tapsLabel: UILabel!
    
    
    @IBAction func playButtonPressed(sender: UIButton!) {
        
        if howManyTapsTextField.text != nil && howManyTapsTextField.text != ""{
            
            logoImage.hidden = true
            howManyTapsTextField.hidden = true
            playButton.hidden = true
            
            tapButton.hidden = false
            tapsLabel.hidden = false
            
            
            maxTapsAmount = Int(howManyTapsTextField.text!)!
            currentTaps = 0
            
            updateTapsLabel()
        }
        
    }
    
    @IBAction func coinButtonTapped(sender: UIButton){
        currentTaps += 1
        
        updateTapsLabel()
        
        if isGameOver(){
            
            resetGame()
        }
    }
    
    func updateTapsLabel(){
        tapsLabel.text = "\(currentTaps) Taps"
    }
    
    func isGameOver() -> Bool{
        if currentTaps == maxTapsAmount{
            return true
        }
        else{
            return false
        }
    }
    
    func resetGame(){
        maxTapsAmount = 0
        currentTaps = 0
        howManyTapsTextField.text = ""
        
        logoImage.hidden = false
        howManyTapsTextField.hidden = false
        playButton.hidden = false
        
        tapButton.hidden = true
        tapsLabel.hidden = true
    }
}

